package com.hackthon.PortfolioManagement.repository;


import com.hackthon.PortfolioManagement.model.Portfolio;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface PortfolioRepository extends MongoRepository<Portfolio, String> {
}
