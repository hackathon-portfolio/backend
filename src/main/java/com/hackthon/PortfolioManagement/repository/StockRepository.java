package com.hackthon.PortfolioManagement.repository;

import com.hackthon.PortfolioManagement.model.Stock;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface StockRepository extends MongoRepository<Stock, String> {
}
