package com.hackthon.PortfolioManagement.service;

import com.hackthon.PortfolioManagement.model.Stock;
import com.hackthon.PortfolioManagement.model.User;
import com.hackthon.PortfolioManagement.repository.StockRepository;
import com.hackthon.PortfolioManagement.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class UserService implements  UserServiceInterface {
    @Autowired
    private UserRepository userRepository;

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public Optional<User> findById(String id) {
        return userRepository.findById(id);
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public User update (User user,String id){
        user.setId(id);
        return userRepository.save(user);
    }

    public void delete (String id) { userRepository.deleteById(id);}
}
