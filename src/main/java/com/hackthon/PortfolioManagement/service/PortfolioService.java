package com.hackthon.PortfolioManagement.service;

import com.hackthon.PortfolioManagement.model.Portfolio;
import com.hackthon.PortfolioManagement.model.Stock;
import com.hackthon.PortfolioManagement.repository.PortfolioRepository;
import com.hackthon.PortfolioManagement.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sound.sampled.Port;
import java.util.List;
import java.util.Optional;

@Component
public class PortfolioService  implements PortfolioServiceInterface{

    @Autowired
    private PortfolioRepository portfolioRepository;

    public List<Portfolio> findAll() {
        return portfolioRepository.findAll();
    }

    public Optional<Portfolio> findById(String id) {
        return portfolioRepository.findById(id);
    }

    public Portfolio save(Portfolio portfolio) {
        return portfolioRepository.save(portfolio);
    }

    public Portfolio update (Portfolio portfolio,String id){
        portfolio.setId(id);
        return portfolioRepository.save(portfolio);
    }

    public void delete (String id) { portfolioRepository.deleteById(id);}

}
