package com.hackthon.PortfolioManagement.service;

import com.hackthon.PortfolioManagement.model.Portfolio;


import java.util.List;
import java.util.Optional;

public interface PortfolioServiceInterface {

    List<Portfolio> findAll();

    Optional<Portfolio> findById(String id);

    Portfolio save(Portfolio portfolio);

    Portfolio update(Portfolio portfolio,String id);

    void delete (String id);

}
