package com.hackthon.PortfolioManagement.service;

import com.hackthon.PortfolioManagement.model.Stock;

import java.util.List;
import java.util.Optional;

public interface StockServiceInterface {
    List<Stock> findAll();

    Optional<Stock> findById(String id);

    Stock save(Stock stock);

    Stock update(Stock stock,String id);

    void delete(String id);
}
