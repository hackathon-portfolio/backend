package com.hackthon.PortfolioManagement.service;

import com.hackthon.PortfolioManagement.model.Stock;
import com.hackthon.PortfolioManagement.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class StockService implements StockServiceInterface{

    @Autowired
    private StockRepository stockRepository;

    public List<Stock> findAll() {
        return stockRepository.findAll();
    }

    public Optional<Stock> findById(String id) {
        return stockRepository.findById(id);
    }

    public Stock save(Stock stock) {
        return stockRepository.save(stock);
    }

    public Stock update (Stock stock,String id){
        stock.setId(id);
        return stockRepository.save(stock);
    }

    public void delete (String id) { stockRepository.deleteById(id);}
}
