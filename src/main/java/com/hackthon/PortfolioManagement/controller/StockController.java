package com.hackthon.PortfolioManagement.controller;

import com.hackthon.PortfolioManagement.model.Stock;
import com.hackthon.PortfolioManagement.service.StockServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;


@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/stock")
public class StockController {

    @Autowired
    private StockServiceInterface stockService;

    /**
     *
     * @return all stocks in database
     */

    @GetMapping
    public List<Stock> findAll() {
        return stockService.findAll();
    }

    /**
     *
     * @param id get stock by id
     * @return
     */
    @GetMapping("{id}")
    public ResponseEntity<Stock> findById(@PathVariable String id) {
        try{
            return new ResponseEntity<Stock>(stockService.findById(id).get(), HttpStatus.OK);
        }
        catch (NoSuchElementException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }

    /**
     *
     * Used to create a new stock
     * @param stock Provide Stock object
     * @return
     */
    @PostMapping
    public Stock save( @RequestBody Stock stock ) {
        return stockService.save(stock);
    }

    /**
     *
     * Used to update an existing with a stockId as a path variable
     * @param stock provide Stock object
     * @return
     */
    @PutMapping("/stocks/{id}")
    public ResponseEntity<Object> update(@RequestBody Stock stock, @PathVariable String id) {

        if (stockService.findById(id).isEmpty())
            return ResponseEntity.notFound().build();
        stockService.update(stock,id);
        return ResponseEntity.noContent().build();
    }

    /**
     *
     * @param id delete Stock object from the database
     * @return
     */
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id) {

        if (stockService.findById(id).isEmpty()){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        stockService.delete(id);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
