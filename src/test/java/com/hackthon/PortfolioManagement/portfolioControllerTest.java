package com.hackthon.PortfolioManagement;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackthon.PortfolioManagement.controller.PortfolioController;
import com.hackthon.PortfolioManagement.model.Portfolio;
import com.hackthon.PortfolioManagement.service.PortfolioServiceInterface;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PortfolioController.class)
public class portfolioControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private PortfolioServiceInterface PortfolioService;

    @Test
    public void testPortfolioControllerFindAll() throws Exception {
        String testId = "1234";

        List<Portfolio> allPortfolio = new ArrayList<Portfolio>();
        Portfolio testPortfolio = new Portfolio();
        testPortfolio.setId(testId);
        allPortfolio.add(testPortfolio);

        when(PortfolioService.findAll()).thenReturn(allPortfolio);

        this.mockMvc.perform(get("/api/v1/portfolio"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));

    }

    @Test
    public void testPortfolioControllerFindById() throws Exception{
        String testId="1234";
        Portfolio testPortfolio=new Portfolio();
        testPortfolio.setId(testId);
        Optional<Portfolio> byId1=Optional.of(testPortfolio);
        when(PortfolioService.findById(testId)).thenReturn(byId1);

        this.mockMvc.perform(get("/api/v1/portfolio/" + testId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));
  }

    @Test
    public void testPortfolioControllerSave() throws Exception{
        String testId="1234";
        Portfolio testPortfolio=new Portfolio();
        testPortfolio.setId(testId);
        when(PortfolioService.save(testPortfolio)).thenReturn(testPortfolio);

        this.mockMvc.perform(post("/api/v1/portfolio").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testPortfolio)))
                .andDo(print())
                .andExpect(status().is(200));
                //.andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));
    }
    @Test
    public void testPortfolioControllerUpdate() throws Exception{
        String testId="1234";
       // String testId1="12345";
        Portfolio testPortfolio=new Portfolio();
        testPortfolio.setId(testId);

        when(PortfolioService.findById(testId)).thenReturn(Optional.of(testPortfolio));
        when(PortfolioService.update(testPortfolio,testId)).thenReturn(testPortfolio);

        this.mockMvc.perform(put("/api/v1/portfolio/portfolios/" + testId).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testPortfolio)))
                .andDo(print())
                .andExpect(status().is(204));
        //.andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));
    }

    @Test
    public void testPortfolioControllerDelete() throws Exception{
        String testId="1234";
        Portfolio testPortfolio=new Portfolio();
        testPortfolio.setId(testId);

        when(PortfolioService.findById(testId)).thenReturn(Optional.of(testPortfolio));
        when(PortfolioService.update(testPortfolio,testId)).thenReturn(testPortfolio);

        this.mockMvc.perform(delete("/api/v1/portfolio/" + testId))
                .andDo(print())
                .andExpect(status().is(204));
        //.andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));
    }
}
