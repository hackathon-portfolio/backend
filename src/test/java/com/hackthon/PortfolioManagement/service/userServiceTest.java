package com.hackthon.PortfolioManagement.service;

import com.hackthon.PortfolioManagement.model.User;
import com.hackthon.PortfolioManagement.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class userServiceTest {
    @Autowired
    private UserService UserService;

    @MockBean
    private UserRepository UserRepository;

    @Test
    public void testUserServiceFindAll() {
        List<User> allUser = new ArrayList<User>();
        User testUser = new User();
        testUser.setId("12345");
        Optional<User> byId1=Optional.of(testUser);
        allUser.add(testUser);

        when(UserRepository.findAll()).thenReturn(allUser);
        when(UserRepository.findById("12345")).thenReturn(byId1);

        assertEquals(1, UserService.findAll().size());
        assertEquals(testUser,UserService.findById("12345").get());
    }

    @Test
    public void testSave()
    {
        User testUser=new User();
        testUser.setId("12345");
        when(UserRepository.save(testUser)).thenReturn(testUser);
        assertEquals(testUser,UserService.save(testUser));
    }

    @Test
    public void testUpdate()
    {
        User testUser = new User();
        testUser.setId("12345");
        when(UserRepository.save(testUser)).thenReturn(testUser);
        assertEquals(testUser,UserService.update(testUser,"12345"));


    }



}
