package com.hackthon.PortfolioManagement.service;

import com.hackthon.PortfolioManagement.model.Stock;
import com.hackthon.PortfolioManagement.repository.StockRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class stockServiceTest {
    @Autowired
    private StockService stockService;

    @MockBean
    private StockRepository stockRepository;

    @Test
    public void testStockServiceFindAll() {
        List<Stock> allStock = new ArrayList<Stock>();
        Stock testStock = new Stock();
        testStock.setId("12345");
        Optional<Stock> byId1=Optional.of(testStock);
        allStock.add(testStock);

        when(stockRepository.findAll()).thenReturn(allStock);
        when(stockRepository.findById("12345")).thenReturn(byId1);

        assertEquals(1, stockService.findAll().size());
        assertEquals(testStock,stockService.findById("12345").get());
    }

    @Test
    public void testSave()
    {
        Stock testStock=new Stock();
        testStock.setId("12345");
        when(stockRepository.save(testStock)).thenReturn(testStock);
        assertEquals(testStock,stockService.save(testStock));
    }

    @Test
    public void testUpdate()
    {
        Stock testStock = new Stock();
        testStock.setId("12345");
        when(stockRepository.save(testStock)).thenReturn(testStock);
        assertEquals(testStock,stockService.update(testStock,"12345"));


    }



}
