package com.hackthon.PortfolioManagement.service;

import com.hackthon.PortfolioManagement.model.Portfolio;
import com.hackthon.PortfolioManagement.repository.PortfolioRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class portfolioServiceTest {
    @Autowired
    private PortfolioService PortfolioService;

    @MockBean
    private PortfolioRepository PortfolioRepository;

    @Test
    public void testPortfolioServiceFindAll() {
        List<Portfolio> allPortfolio = new ArrayList<Portfolio>();
        Portfolio testPortfolio = new Portfolio();
        testPortfolio.setId("12345");
        Optional<Portfolio> byId1=Optional.of(testPortfolio);
        allPortfolio.add(testPortfolio);

        when(PortfolioRepository.findAll()).thenReturn(allPortfolio);
        when(PortfolioRepository.findById("12345")).thenReturn(byId1);

        assertEquals(1, PortfolioService.findAll().size());
        assertEquals(testPortfolio,PortfolioService.findById("12345").get());
    }

    @Test
    public void testSave()
    {
        Portfolio testPortfolio=new Portfolio();
        testPortfolio.setId("12345");
        when(PortfolioRepository.save(testPortfolio)).thenReturn(testPortfolio);
        assertEquals(testPortfolio,PortfolioService.save(testPortfolio));
    }

    @Test
    public void testUpdate()
    {
        Portfolio testPortfolio = new Portfolio();
        testPortfolio.setId("12345");
        when(PortfolioRepository.save(testPortfolio)).thenReturn(testPortfolio);
        assertEquals(testPortfolio,PortfolioService.update(testPortfolio,"12345"));


    }



}
