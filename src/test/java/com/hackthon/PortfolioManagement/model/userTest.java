package com.hackthon.PortfolioManagement.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class userTest {

    @BeforeEach
    public void runBeforeEachTest() {
        System.out.println("this runs first!!");
    }


    @Test
    public void testPokemonGettersSetters() {
        User testUser = new User();
        testUser.setId("12345");
        testUser.setUsername("AAA");


        assertEquals("12345", testUser.getId());
        assertEquals("AAA", testUser.getUsername());
    }
}


