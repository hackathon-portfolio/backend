package com.hackthon.PortfolioManagement.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class portfolioTest {

    @BeforeEach
    public void runBeforeEachTest() {
        System.out.println("this runs first!!");
    }


    @Test
    public void testPortfolioGettersSetters() {
        Portfolio testPortfolio = new Portfolio();
        testPortfolio.setId("12345");
        testPortfolio.setUserId("AAA");
       List<Stock> stockList=new ArrayList<>();
       Stock stock=new Stock();
       stock.setId("0810");
       stockList.add(stock);
       testPortfolio.setStockList(stockList);


       assertEquals(stockList,testPortfolio.getStockList());
        assertEquals("12345", testPortfolio.getId());
        assertEquals("AAA", testPortfolio.getUserId());

    }
}


