package com.hackthon.PortfolioManagement;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackthon.PortfolioManagement.controller.UserController;
import com.hackthon.PortfolioManagement.model.User;
import com.hackthon.PortfolioManagement.service.UserServiceInterface;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
public class userControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserServiceInterface UserService;

    @Test
    public void testUserControllerFindAll() throws Exception {
        String testId = "1234";

        List<User> allUser = new ArrayList<User>();
        User testUser = new User();
        testUser.setId(testId);
        allUser.add(testUser);

        when(UserService.findAll()).thenReturn(allUser);

        this.mockMvc.perform(get("/api/v1/user"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));

    }

    @Test
    public void testUserControllerFindById() throws Exception{
        String testId="1234";
        User testUser=new User();
        testUser.setId(testId);
        Optional<User> byId1=Optional.of(testUser);
        when(UserService.findById(testId)).thenReturn(byId1);

        this.mockMvc.perform(get("/api/v1/user/" + testId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));
  }

    @Test
    public void testUserControllerSave() throws Exception{
        String testId="1234";
        User testUser=new User();
        testUser.setId(testId);
        when(UserService.save(testUser)).thenReturn(testUser);

        this.mockMvc.perform(post("/api/v1/user").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testUser)))
                .andDo(print())
                .andExpect(status().is(200));
                //.andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));
    }

    @Test
    public void testUserControllerUpdate() throws Exception{
        String testId="1234";
       // String testId1="12345";
        User testUser=new User();
        testUser.setId(testId);

        when(UserService.findById(testId)).thenReturn(Optional.of(testUser));
        when(UserService.update(testUser,testId)).thenReturn(testUser);

        this.mockMvc.perform(put("/api/v1/user/users/" + testId).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testUser)))
                .andDo(print())
                .andExpect(status().is(204));
        //.andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));
    }

    @Test
    public void testUserControllerDelete() throws Exception{
        String testId="1234";
        User testUser=new User();
        testUser.setId(testId);

        when(UserService.findById(testId)).thenReturn(Optional.of(testUser));
        when(UserService.update(testUser,testId)).thenReturn(testUser);

        this.mockMvc.perform(delete("/api/v1/user/" + testId))
                .andDo(print())
                .andExpect(status().is(204));
        //.andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));
    }
}
